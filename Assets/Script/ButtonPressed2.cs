﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonPressed2 : MonoBehaviour,IPointerDownHandler, IPointerUpHandler {

    public Pesawat pesawat;

    void Start()
    {

    }
    void Update()
    {
        if (!ispressed)
            return;
        // DO SOMETHING HERE
        Kiri();

        Debug.Log("Pressed Up");
    }
    bool ispressed = false;
    public void OnPointerDown(PointerEventData eventData)
    {
        ispressed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        ispressed = false;
    }

    public void Atas()
    {
        pesawat.MoveUp();
    }
    
    public void Bawah()
    {
        pesawat.MoveDown();
    }

    public void Kiri()
    {
        pesawat.MoveLeft();
    }

    public void Kanan()
    {
        pesawat.MoveRight();
    }

    public void Tembak()
    {
        pesawat.Attack();
    }
    
}
